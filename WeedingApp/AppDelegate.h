//
//  AppDelegate.h
//  WeedingApp
//
//  Created by MacBook Pro on 05.03.2014.
//  Copyright (c) 2014 Cristian Petra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
